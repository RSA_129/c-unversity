﻿//Создать класс «Однонаправленный список» с указателем на начало списка, элементами которого являются экземпляры класса «Преподаватель».
//Класс «Преподаватель» включает поля : фамилия преподавателя, наименование кафедры, количество часов лекций, количество часов практик.
//Для каждой кафедры определить преподавателя с максимальной суммарной загрузкой.

#include <iostream> 
#include <string> // библиотека для работы со строками
#include <forward_list> // библиотека для создания односвязного списка
#include <map> // библиотека для создания словарей/карт 


using namespace std;
class Teacher // главный класс "Преподаватель"
{
	string lastname, department;
	int lecture_h, practice_h;
public:
	Teacher(string ln, string d, int lec, int pr) {
		lastname = ln;
		department = d;
		lecture_h = lec;
		practice_h = pr;
	}
	string nameget() {
		return lastname;
	}
	string depget() {
		return department;
	}
	int lecture_hget() {
		return lecture_h;
	}
	int practice_hget() {
		return practice_h;
	}
};
string inputstring() // функция для ввода непустой строки
{
	string s;
	while (true) {
		getline(cin, s);  _flushall();
		if (s.empty())
			cout << "Incorrect input, type correct\n>> ";
		else
			return s;
	}
}
int inputchecknums() // функция для ввода целых чисел
{
	string s;
	int i;
	while (true)
	{
		s = inputstring();
		for (i = 0; s[i]; i++) // проверка, только ли цифры в строке
		{
			if (!isdigit(s[i]))
			{
				break;
			}
		}
		if (i == s.size())
			return atoi(s.c_str()); // преобразование в тип int
		else
		{
			cout << "Incorrect input, type correct\n>> ";
		}
	}
}
void info_teachers(forward_list <Teacher> list) // функция для вывода всех учителей
{
	int i = 0;
	// проход с помощью итератора по всем элементам списка
	for (forward_list <Teacher> ::iterator it = list.begin(); it != list.end(); it++) {
		cout << i + 1 << ". Teacher's lastname - " << it->nameget() << endl;
		cout << "Department - " << it->depget() << endl;
		cout << "Hours of lectures - " << it->lecture_hget() << endl;
		cout << "Hours of practice - " << it->practice_hget() << endl;
		i++;
	}
	cout << endl;
}
void new_teacher(forward_list <Teacher>& list) // функция для добавления нового учителя
{
	string lastname, department;
	int lecture_h, practice_h;
	cout << "Type Department >> ";
	department = inputstring();
	cout << "Type lastname >> ";
	lastname = inputstring();
	cout << "Type hours of lectures >> ";
	lecture_h = inputchecknums();
	cout << "Type hours of practice >> ";
	practice_h = inputchecknums();
	// добавляю в начало, потому что в конец это делать более затратно
	list.push_front(Teacher(lastname, department, lecture_h, practice_h));
	cout << "Teacher was added to the list\n";
	cout << endl;
}
void del_teacher(forward_list <Teacher>& list) // функция для удаления преподавателя
{
	string lastname, department;
	forward_list <Teacher> ::iterator prev; // т.к. мы не можем стереть текущий элемент, поэтому
	// надо запоминать предыдущий
	cout << "Type Department >> ";
	department = inputstring();
	cout << "Type lastname >> ";
	lastname = inputstring();
	int check = 0;
	for (forward_list <Teacher> ::iterator it = list.begin(); it != list.end(); it++) {
		if (it->nameget() == lastname && it->depget() == department) {
			check = 1;
			if (it == list.begin())
				list.pop_front();
			else
				list.erase_after(prev);
			printf("Teacher was successfully deleted\n");
			break;
		}
		prev = it;
	}
	if (check == 0) {
		printf("Teacher isn't in the list\n");
	}
	cout << endl;
}
void mx_of_each_dep(forward_list <Teacher> list) // функция для вывода учителей по условию
{
	// словари для записи кол-ва часов у кафедры и учителя с этими часами
	map <string, int> hours_top; map <string, string> top_teachers;
	string department, lastname; int lec, pr;
	for (forward_list <Teacher> ::iterator it = list.begin(); it != list.end(); it++) {
		department = it->depget(); lastname = it->nameget();
		lec = it->lecture_hget(), pr = it->practice_hget();
		if (top_teachers[department].empty()) // если пустой, то добавляю к каждый список значения
		{
			top_teachers[department] = lastname;
			hours_top[department] = lec + pr;
		}
		else if (hours_top[department] < lec + pr) // нахожу макс. кол-во часов у каждой кафедры
		{

			top_teachers[department] = lastname;
			hours_top[department] = lec + pr;
		}
	}
	cout << "There's a list of teachers with max hours total: " << endl;
	// вывод также, как и список, через итераторы
	for (map <string, string> ::iterator it = top_teachers.begin(); it != top_teachers.end(); it++) {
		cout << "Department - " << it->first << ", teacher - " << it->second <<
			", hours - " << hours_top[it->first] << endl;
	}
	cout << endl;
}
void menu(forward_list <Teacher> list) // меню
{
	while (true) {
		cout << "Choose:" << endl;
		cout << "Type 0 to quit" << endl;
		cout << "Type 1 to see the list of all teacher" << endl;
		cout << "Type 2 to create new teacher" << endl;
		cout << "Type 3 to delete teacher from the list" << endl;
		cout << "Type 4 to see teacher of each dep. with highest total load" << endl;
		cout << ">> ";
		int choice = inputchecknums();
		if (choice == 1)
			info_teachers(list);
		else if (choice == 2)
			new_teacher(list);
		else if (choice == 3)
			del_teacher(list);
		else if (choice == 4)
			mx_of_each_dep(list);
		else if (choice == 0) {
			cout << "Exit done\n";
			break;
		}
		else
			cout << "Incorrect number. Type number from the list\n\n";
	}

}

int main() // главная функция
{
	forward_list <Teacher> list; // создания односвязного списка
	list.push_front(Teacher("Dab", "Uwu", 70, 90)); // добавление в список
	list.push_front(Teacher("Roma", "IB", 228, 1337));
	list.push_front(Teacher("Umwatchaseeei", "Uwu", 56, 19));
	list.push_front(Teacher("Amerika", "Uwu", 75, 95));
	list.push_front(Teacher("Russkiy", "Netblinamerikanetz", 420, 911));
	list.push_front(Teacher("Ukraina", "IB", 10, 10));
	menu(list);
	return 0;
}
