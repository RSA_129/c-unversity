﻿//Создать класс «Очередь» на основе однонаправленного списка, элементами которого являются экземпляры класса «Станции».
//Класс «Станции» включает поля : название станции, номер поезда, время прибытия, время стоянки
//Определить номера и время отправления поездов, которыми можно доехать до заданной станции

#include <iostream> 
#include <string> // библиотека для работы со строками
#include <vector> // библиотека для создания динамического массива
#include <array> // библиотека для создания статического массива 
#include <queue> // библиотека для работы с очередью
#include <ctime> // библиотека для работы с очень удобным временем


using namespace std;
class Station // главный класс "Станция"
{
	string name;
	array <int, 2> arrive_time;
	int train_num, waiting_time;
public:
	Station(string nm, int tr, array <int, 2> at, int wt) {
		name = nm;
		train_num = tr;
		arrive_time = at;
		waiting_time = wt;
	}
	string nameget() {
		return name;
	}
	int numget() {
		return train_num;
	}
	array <int, 2> a_t_get() {
		return arrive_time;
	}
	int w_t_get() {
		return waiting_time;
	}
	~Station() {}
};
string inputstring() // функция для ввода непустой строки
{
	string s;
	while (true) {
		getline(cin, s);  _flushall();
		if (s.empty())
			cout << "Incorrect input, type correct\n>> ";
		else
			return s;
	}
}
int inputchecknums(int maks = 0) // функция для ввода целых чисел
{
	string str;
	int i, s;
	bool check = false;
	while (true)
	{
		str = inputstring();
		for (i = 0; str[i]; i++) // проверка, только ли цифры в строке
		{
			if (!isdigit(str[i]))
			{
				break;
			}
		}
		if (i == str.size()) { // это проверка нужна при вводе времени
			s = atoi(str.c_str()); // преобразование в тип int
			if (maks == 0) { check = true; }
			else {
				if (s >= 0 && s < maks) { check = true; }
			}
		}
		if (check)
			return s;
		else
			cout << "Incorrect input, type correct\n>> ";
	}
}
string _add_minutes(array <int, 2> arr, int minutes) // функция для добавления времени и вывода строки
{
	struct tm t; time_t rawtime; char out[100];
	time(&rawtime); localtime_s(&t, &rawtime);
	t.tm_hour = arr[0]; t.tm_min = arr[1]; t.tm_sec = 0;
	rawtime = mktime(&t); rawtime += 60 * minutes;
	localtime_s(&t, &rawtime); strftime(out, 10, "%H:%M", &t);
	string s = out;
	return s;
}
void list_stations(queue <Station> q_temp) // функция для вывода всех станций
{
	vector <string>	st_ns; string curr_st; int i;
	cout << "Here's the list of stations:" << endl;
	while (!q_temp.empty()) // проход по очереди с проверкой на повторение станций
	{
		curr_st = q_temp.front().nameget();
		if (st_ns.empty()) {
			cout << "- " << curr_st << endl;
			st_ns.push_back(curr_st);
		}
		else {
			for (i = 0; i < st_ns.size(); i++) {
				if (curr_st == st_ns[i]) { break; }
			}
			if (i == st_ns.size()) {
				cout << "- " << curr_st << endl;
				st_ns.push_back(curr_st);
			}
		}
		q_temp.pop();
	}
	cout << endl;
}
void add_station(queue <Station>& q) // функция для добавления станций
{
	string name;
	int train_num, waiting_time, hour, min;
	cout << "Type name of the station:\n>> "; name = inputstring();
	cout << "Type train number:\n>> "; train_num = inputchecknums();
	cout << "Type hour of arrival time:\n>> "; hour = inputchecknums(24);
	cout << "Type minute of arrival time:\n>> "; min = inputchecknums(60);
	cout << "Type minutes of waiting:\n>> "; waiting_time = inputchecknums(1400);
	q.push(Station(name, train_num, array <int, 2> {hour, min}, waiting_time));
	cout << "Station was added successfully" << endl;
	cout << endl;
}
void dl_station(queue <Station>& q) // функция для удаления станций
{
	string name, curr_st; bool check = false;
	queue <Station> temp;
	cout << "Type name of the station:\n>> "; name = inputstring();
	while (!q.empty()) {
		curr_st = q.front().nameget();
		if (curr_st == name) {
			check = true;
		}
		else {
			temp.push(Station(curr_st, q.front().numget(), q.front().a_t_get(), q.front().w_t_get()));
		}
		q.pop();
	}
	q = temp;
	if (check)
		printf("Station was successfully deleted\n");
	else
		printf("Station isn't in the list\n");
	cout << endl;
}

void departures(queue <Station> q_temp) // функция для вывода расписания
{
	string curr_st, nm_station; bool check = false;
	cout << "Type name of the station:\n>> "; nm_station = inputstring();
	while (!q_temp.empty())
	{
		curr_st = q_temp.front().nameget();
		if (curr_st == nm_station) {
			if (!check) {
				cout << "Here's list of trains and waiting time:\n";
				check = true;
			}

			printf("- Train num: %d | Arriving/Departure time: %s-%s\n", q_temp.front().numget(),
				_add_minutes({ q_temp.front().a_t_get()[0] , q_temp.front().a_t_get()[1] }, 0).c_str(),
				_add_minutes({ q_temp.front().a_t_get()[0] , q_temp.front().a_t_get()[1] }, q_temp.front().w_t_get()).c_str());
		}
		q_temp.pop();
	}
	if (!check)
		cout << "There's no such station in the list\n";
	cout << endl;
}
void menu(queue <Station>& q) // меню
{
	while (true) {
		cout << "Choose:" << endl;
		cout << "Type 0 to quit" << endl;
		cout << "Type 1 to see the list of all stations" << endl;
		cout << "Type 2 to add new station" << endl;
		cout << "Type 3 to delete station from the list" << endl;
		cout << "Type 4 to see timetable of the station" << endl;
		cout << ">> ";
		int choice = inputchecknums();
		if (choice == 1)
			list_stations(q);
		else if (choice == 2)
			add_station(q);
		else if (choice == 3)
			dl_station(q);
		else if (choice == 4)
			departures(q);
		else if (choice == 0) {
			cout << "Exit done\n";
			break;
		}
		else
			cout << "Incorrect number. Type number from the list\n\n";
	}

}

int main() // главная функция
{
	queue <Station> q;
	q.push(Station("Rea", 456, { 12, 35 }, 5));
	q.push(Station("Yooops", 123, { 15, 20 }, 10));
	q.push(Station("Rea", 457, { 12, 01 }, 15));
	q.push(Station("Y", 366, { 0, 0 }, 5));
	q.push(Station("Rea", 499, { 14, 59 }, 15));
	q.push(Station("Y", 390, { 1, 30 }, 90));
	q.push(Station("Y", 457, { 4, 20 }, 228));
	menu(q);
	return 0;
}
