﻿//Создать класс «Двунаправленный список», элементами которого являются экземпляры класса «Тур».
//Класс «Турпоездки» включает поля : название турагентства, название тура, количество дней, стоимость одного дня.
//Определить названия агентств, у которых итоговая стоимость больше половины туров дороже заданного значения

#include <iostream>
#include <string>
#include <list>
#include <map>
#include <vector>

using namespace std;
// главный класс тур
class Tour
{
	string agency_name, tour_name;
	int days, price_per_day;

public:
	Tour(string nm, string nm2, int d, int price) {
		agency_name = nm;
		tour_name = nm2;
		days = d;
		price_per_day = price;
	}
	string anameGet() {
		return agency_name;
	}
	string tnameGet() {
		return tour_name;
	}
	int daysGet() {
		return days;
	}
	int priceGet() {
		return price_per_day;
	}
	int total_price() {
		return days * price_per_day;
	}
};
string Inputstr() // функция, для правильного ввода строки
{
	string s;
	while (true) {
		getline(cin, s);  _flushall();
		if (s.empty())
			cout << "Incorrect input, type correct: ";
		else
			return s;
	}
}
int Inputcheckint() // функция, для правильного ввода целых чисел
{
	string s;
	while (true)
	{
		s = Inputstr();
		if (s.find_first_not_of("0123456798") == string::npos && not s.empty())
			return atoi(s.c_str());
		else
		{
			cout << "Incorrect input, type correct: ";
		}
	}
}

void info_agencies(list <Tour> tours) // функция, для вывода агенств
{
	int i = 0;
	for (list <Tour> ::iterator it = tours.begin(); it != tours.end(); it++) {
		cout << i + 1 << ") Agency name: " << it->anameGet() << endl;
		cout << "Tour name : " << it->tnameGet() << endl;
		cout << "Days : " << it->daysGet() << endl;
		cout << "Price per day: " << it->priceGet() << endl;
		cout << "Total price: " << it->total_price() << endl;
		i++;
	}
	cout << endl;
}
void add_tour(list <Tour>& tours) // функция, для добавления тура
{
	string agency_name, tour_name;
	int days, price_per_day;
	cout << "Type agency name: "; agency_name = Inputstr();
	cout << "Type tour name: "; tour_name = Inputstr();
	cout << "Type amount of days: "; days = Inputcheckint();
	cout << "Type price per day: "; price_per_day = Inputcheckint();
	tours.push_back(Tour(agency_name, tour_name, days, price_per_day));
	cout << tour_name << " was added susuccessfully" << endl;
	cout << endl;
}
void l_tour(list <Tour>& tours) // функция, для удаления тура
{
	string agency_name, tour_name;
	cout << "Type agency name: "; agency_name = Inputstr();
	cout << "Type tour name: "; tour_name = Inputstr();
	bool check = false;
	for (list <Tour> ::iterator it = tours.begin(); it != tours.end(); it++) {
		if (it->anameGet() == agency_name && it->tnameGet() == tour_name) {
			tours.erase(it);
			check = true;
			printf("Tour %s of %s company was deleted successfully\n", tour_name.c_str(),
				agency_name.c_str());
			break;
		}
	}
	if (not check) {
		printf("Tour %s of %s company cannot be found\n", tour_name.c_str(),
			agency_name.c_str());
	}
	cout << endl;
}
void high_price(list <Tour>& tours) // функция, для вывода агенст по условию
{
	map  <string, vector<int>>  agencies; vector <string> names;
	cout << "Type value of price: "; int price = Inputcheckint();
	string str = "";
	for (list <Tour> ::iterator it = tours.begin(); it != tours.end(); it++) {
		string aname = it->anameGet();
		if (agencies[aname].empty()) // если пустое, то создаю массив для данного имени
		{
			agencies[aname] = { 0,0 }; // amount of higher price/total
			names.push_back(aname);
		}
		agencies[aname][1] += 1;
		if (it->total_price() > price) {
			agencies[aname][0] += 1;
		}
	}
	for (int i = 0; i < names.size(); i++)
	{
		float fl_total = agencies[names[i]][1]; // преобразую тип для проверки
		if (agencies[names[i]][0] / fl_total > 0.5)
		{
			str.append(names[i]); str.append(", "); // если больше половины туров
			// у агенства подходят, то добавляю в строку для вывода
		}
	}
	if (not str.empty()) { // проверка пустоты строки
		str.erase(str.size() - 2, 2);
		str.insert(0, "Here's premium travel agencies: ");
	}
	else
		str.append("There is no premium travel agencies");
	cout << str << endl;
	cout << endl;
}
void MainMenu(list <Tour>& tours) // гл. меню
{
	while (true) {
		cout << "Choose an option:\n";
		cout << "Type 1 to see available tours\n";
		cout << "Type 2 to add new tour\n";
		cout << "Type 3 to delete tour\n";
		cout << "Type 4 to see high price travel agencies\n";
		cout << "Type 0 to exit\n";
		cout << ">> ";
		int choice = Inputcheckint();
		if (choice == 1)
			info_agencies(tours);
		else if (choice == 2)
			add_tour(tours);
		else if (choice == 3)
			l_tour(tours);
		else if (choice == 4)
			high_price(tours);
		else if (choice == 0) {
			cout << "Exit done\n";
			break;
		}
		else
			cout << "Incorrect input. Type correct\n";
	}

}
int main()
{
	list <Tour> tours;
	tours.push_back(Tour("Arbuz", "Papa??", 10, 100)); // 1000
	tours.push_back(Tour("Rarka", "A", 2, 500)); // 1000
	tours.push_back(Tour("Rarka", "B", 3, 500)); // 1500
	tours.push_back(Tour("Rarka", "D", 1, 500)); // 500
	tours.push_back(Tour("Arbuz", "Mama??", 6, 500)); // 3000
	tours.push_back(Tour("Rarka", "E", 9, 500)); // 4500
	tours.push_back(Tour("Arbuz", "Baba??", 4, 600)); // 2400
	tours.push_back(Tour("Rarka", "Y", 5, 500)); // 2500
	MainMenu(tours);
	return 0;
}

